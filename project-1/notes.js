console.log('NOTES.JS LOG!');

// используем exports
// console.log(module);

module.exports.age = 25;

// тоже самое что и function(){}
module.exports.addNote = () => {
    console.log('arrow function');
    return 'New note';
};

/**
 * Сумирует два числа. Не более того.
 * @param {*} a 
 * @param {*} b 
 */
module.exports.sum = (a, b) => {
    return a + b;
};