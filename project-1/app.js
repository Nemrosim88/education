console.log('Hello log');

const fs = require('fs');
const os = require('os');
const _ = require('lodash');


// -----------------  LODASH -----------------------

console.log(_.isString(true));
console.log(_.isString('Hello'));

var filteredArray = _.uniq(['one', 'three', 'two', 1, 1, 1, 2, 3, 4]);
console.log(filteredArray);

// -----------------  NOTES -----------------------


//relative path to file notes.js
const notes = require('./notes');

var result = notes.addNote();
console.log(result);

var resultSum = notes.sum(25, 5);
console.log(resultSum);


// Original line
//fs.appendFile('text.txt', 'Hello world');






// -----------------  OS -----------------------


var user = os.userInfo();
// console.log(user);




// -----------------  FS -----------------------


// Option one
fs.appendFile('text.txt', 'Hello ' + user.username + '!', function (err) {
    if (err) {
        console.log('Unable to write to file');
    }
});

// Option one v2
fs.appendFile('text.txt', `\n Hello ${user.username} BLAH BLAH! You are ${notes.age}`, function (err) {
    if (err) {
        console.log('Unable to write to file');
    }
});

//Option two
fs.appendFileSync('text.txt', 'Hello world\n');