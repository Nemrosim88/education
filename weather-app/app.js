const yargs = require('yargs');
const geocode = require('./geocode');
const weather = require('./weather');
const request = require('request');

const args = yargs.options({
    city: {
        demand: true,
        alias: 'city',
        describe: 'Address to fetch weather to',
        string: true
    }
})
    .help()
    .alias('help', 'h')
    .argv;

console.log(args);
var encodedAddress = encodeURIComponent(args.city);

geocode.geocodeAddress(encodedAddress, (errorMessage, results) => {
    if (errorMessage) {
        console.log('Error message');
    } else {
        console.log(JSON.stringify(results.city, undefined, 2));

        weather.getWeather(results.latLng_lat, results.latLng_lng);
       

    }




});


// db1a46b2c4e94f889b0a2f08722d162e
// https://api.darksky.net/forecast/db1a46b2c4e94f889b0a2f08722d162e/37.8267,-122.4233