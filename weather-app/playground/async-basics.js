//1
console.log('Starting app...');

//4 async-callback
setTimeout(() => {
    console.log('Two seconds!');
    // 2 секунды
}, 2000);

//3 async-callback
setTimeout(() => {
    console.log('No delay!');
    // 2 секунды
}, 0);

//2
console.log('Finishing app...');
