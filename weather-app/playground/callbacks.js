var getUser = (id, callback) => {
    var user = {
        id: id,
        name: 'Vikram'
    };

    setTimeout(() => {
        console.log('Delay');
        callback(user);
    }, 3000);
   
};

getUser(34, function (a) {
    console.log(a);
});

console.log('Finish');