const request = require('request');

// https://darksky.net/dev/account   ----> Температура
module.exports.getWeather = function (lat, lng, callback) {
    request(
        {
            url: `https://api.darksky.net/forecast/db1a46b2c4e94f889b0a2f08722d162e/${lat},${lng}`,
            json: true
        },
        function (error, response, body) {
            console.log('error:', error);
            console.log('statusCode:', response && response.statusCode);
            console.log('Timezone:', JSON.stringify(body.timezone, undefined, 3));
            // Фар в Цельс
            var temp = (5 / 9) * (body.currently.temperature - 32)

            console.log('Температура:', JSON.stringify(Math.round(temp), undefined, 3));

            callback(undefined, {
                temperature: temp
            })
        });
};