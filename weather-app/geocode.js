const request = require('request');

var geocodeAddress = function (encodedAddress, callback) {
    // Пример с сайта
    // request('http://www.google.com', function (error, response, body) {
    //     console.log('error:', error); // Print the error if one occurred
    //     console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
    //     console.log('body:', body); // Print the HTML for the Google homepage.
    // });

    // http://www.mapquestapi.com/geocoding/v1/address?key=aVFEAdnOwsdewQCr4iiAECG3h1EDsG29&location=1301%20lombard%20street%20philadelphia
    // Вариант 2
    request(
        {
            url: `http://www.mapquestapi.com/geocoding/v1/address?key=aVFEAdnOwsdewQCr4iiAECG3h1EDsG29&street=26+Preobrazhenska&city=${encodedAddress}&postalCode=02000`,
            json: true
        },
        (error, response, body) => {
            if (error) {
                callback('Error');
            } else if (body.status === 'ZERO_RESULTS') {
                callback('Unable to find that address');
            } else if (body.info.statuscode === 0) {
                callback(undefined, {
                    error: error,
                    statusCode: response && response.statusCode,
                    // JSON.stringify(body, undefined, 3) -> форматирует текст к читабельному виду
                    body: body,

                    Улица: `${body.results[0].locations[0].street}`,
                    Район1: `${body.results[0].locations[0].adminArea4}`,
                    Район2: `${body.results[0].locations[0].adminArea6}`,

                    latLng_lat: `${body.results[0].locations[0].latLng.lat}`,
                    latLng_lng: `${body.results[0].locations[0].latLng.lng}`
                })
            }
        });

};

module.exports.geocodeAddress = geocodeAddress;


