console.log('Starting the app...');

const fs = require('fs');
const _ = require('lodash');
const yargs = require('yargs');
const notes = require('./notes');



// node project-2/app.js add --title="secrects 2" --body="This is my secret"

console.log(process.argv);

const argv = yargs.argv;
var command = process.argv[2];

console.log('Command was:', command);
console.log('ARGV was:', argv);

if (command === 'add') {
    var note = notes.addNote(argv.title, argv.body);
}
else if (command === 'list') {
    notes.getAll();
}
else if (command === 'read') {
    notes.getNote(argv.title);
}
else if (command === 'remove') {
    notes.removeNote(argv.title);
}
else {
    console.log('Command not recognized!');
}

