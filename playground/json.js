var obj = {
    name: 'Andrey'
};

var stringObj = JSON.stringify(obj);
console.log(typeof stringObj);
console.log(stringObj);

var personString = '{"name":"Andrey", "age":25}';
var person = JSON.parse(personString);
console.log(typeof person);
console.log(person);

const fs = require('fs');

var origNote = {
    title: 'Sometitle',
    body: 'Some body'
};

var originalNoteString = JSON.stringify(origNote);


fs.writeFileSync('notes.json', originalNoteString);
var noteString = fs.readFileSync('notes.json');
var note = JSON.parse(noteString);

console.log(typeof note);
console.log(note.title);

