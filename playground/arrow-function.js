/**
 * Вариант 1
 * @param  x 
 */
var square = function (x) {
    var result = x * x;
    return result;
};

/**
 * Вариант 2
 * @param  x 
 */
var square_2 = (x) => {
    var result = x * x;
    return result;
};

/**
 * Вариант 3
 * @param  x 
 */
var square_3 = (x) => x * x;

/**
 * Вариант 4
 * @param  x 
 */
var square_4 = x => x * x;

console.log(square(9));
console.log(square_2(9));
console.log(square_3(9));
console.log(square_4(9));

// ===================================================
// ===================================================
// ===================================================

var user = {
    name: 'Artem',
    sayHi: () => {
        console.log('Hello');
    }
};

user.sayHi();

// ============= V2 ===========

var user_v2 = {
    name: 'Artem',
    // в arrow function "this" не сработает
    sayHi: () => {
        // не сработает как надо. нужно использовать след. вариант.
        console.log(arguments);
        console.log(`Hello ${this.name}`);
    },
    sayHiAlt() {
        console.log(arguments);
        console.log(`Hello ${this.name}`);
    }
};

user_v2.sayHi();
user_v2.sayHiAlt();
user_v2.sayHiAlt(1, 2, 3);